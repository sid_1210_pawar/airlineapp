package com.example.airlineapp.util

import android.util.Log
import com.example.airlineapp.BaseClass

class LOG {


    companion object {
        val TAG = "AirlineApp"
        fun e(msg: String) {
            if (BaseClass.isDebugable)
                Log.e(TAG, msg)
        }
    }
}