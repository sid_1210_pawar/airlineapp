package com.example.airlineapp.networking

import com.example.airlineapp.model.AccidentalStatisticsData
import com.example.airlineapp.model.FinByLocationData
import com.example.airlineapp.model.GeographicalData
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {
    @GET("aggregated-stateofoccurrence-stats")
    fun getStateOfOccurence(
        @Query("api_key") api_key: String,
        @Query("states") states: String,
        @Query("format") format: String,
        @Query("Risk") risk: String,
        @Query("Year") year: String
    ): Single<ArrayList<AccidentalStatisticsData>>


    @GET("aggregated-stateofoperator-staats")
    fun getStateOfOperator(
        @Query("api_key") api_key: String,
        @Query("states") states: String,
        @Query("format") format: String,
        @Query("Risk") risk: String,
        @Query("Year") year: String
    ): Single<ArrayList<AccidentalStatisticsData>>


    @GET("flat-stateofoccurrence-stats")
    fun getFlatStateOfOccurence(
        @Query("api_key") api_key: String,
        @Query("states") states: String,
        @Query("format") format: String,
        @Query("Risk") risk: String,
        @Query("Year") year: String
    ): Single<ArrayList<AccidentalStatisticsData>>


    @GET("fir-list")
    fun geGeographaicalRegion(
        @Query("api_key") api_key: String,
        @Query("firs") firs: String,
        @Query("format") format: String
    ): Single<ArrayList<GeographicalData>>


    @GET("fir-find-list")
    fun getByLocation(
        @Query("api_key") api_key: String,
        @Query("format") format: String,
        @Query("places") places : String
    ): Single<ArrayList<FinByLocationData>>
}