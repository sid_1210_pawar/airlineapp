package com.example.airlineapp.networking

import com.example.airlineapp.BaseClass
import com.example.airlineapp.model.AccidentalStatisticsData
import com.example.airlineapp.model.FinByLocationData
import com.example.airlineapp.model.GeographicalData
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.reflect.Array

class ApiService(baseURl: String) {
    private val api = Retrofit.Builder()
        .baseUrl(baseURl)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
        .create(Api::class.java)


    fun getStateOfOccurenceList(
        states: String,
        risk: String,
        year: String
    ): Single<ArrayList<AccidentalStatisticsData>> {
        return api.getStateOfOccurence(BaseClass.api_key, states, "json", risk, year)
    }


    fun getStateOfOperatorist(
        states: String,
        risk: String,
        year: String
    ): Single<ArrayList<AccidentalStatisticsData>> {
        return api.getStateOfOperator(BaseClass.api_key, states, "json", risk, year)

    }

    fun getFlatStateOccurenceList(
        states: String,
        risk: String,
        year: String
    ): Single<ArrayList<AccidentalStatisticsData>> {
        return api.getFlatStateOfOccurence(BaseClass.api_key, states, "json", risk, year)
    }

    fun getFindByLocation(
        places: String
    ): Single<ArrayList<FinByLocationData>> {
        return api.getByLocation(BaseClass.api_key,"json",places)
    }

    fun getGeographicalData(
        firs: String
    ): Single<ArrayList<GeographicalData>> {
        return api.geGeographaicalRegion(BaseClass.api_key,firs,"json")
    }

}