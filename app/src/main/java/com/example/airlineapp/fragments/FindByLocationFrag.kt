package com.example.airlineapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.airlineapp.R
import com.example.airlineapp.adapter.LocationAdapter
import com.example.airlineapp.model.FinByLocationData
import com.example.airlineapp.viewmodels.FindByLocationViewModel
import kotlinx.android.synthetic.main.fragment_aggregated_state_of_occurence.*
import kotlinx.android.synthetic.main.fragment_find_by_location.*

class FindByLocationFrag : Fragment() {


    lateinit var locationViewModel: FindByLocationViewModel
    var locationList = arrayListOf<FinByLocationData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_find_by_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationViewModel = ViewModelProviders.of(this).get(FindByLocationViewModel::class.java)

        locationViewModel.fetchByLocation(placesET.text.toString().trim())

        locationList.let {
            findByLocationRV.adapter =
                context?.let { it1 ->
                    LocationAdapter(it1, it)
                }
        }

        filterBtnLocation.setOnClickListener {
            context?.let { it1 -> locationViewModel.hideKeyboardFrom(it1, it) }
            locationViewModel.fetchByLocation(
                placesET.text.toString()
            )
        }

        resetBtnLocation.setOnClickListener {
            context?.let { it1 -> locationViewModel.hideKeyboardFrom(it1, it) }
            placesET.setText("")
            locationViewModel.fetchByLocation(
                placesET.text.toString()
            )
        }

        observeChanges()
    }


    private fun observeChanges() {

        locationViewModel.loadError.observe(this, Observer { error ->
            error.let {
                noDataFoundLocation.visibility = if (it) View.VISIBLE else View.GONE
            }
        })

        locationViewModel.showProgress.observe(this, Observer { progress ->
            progress.let {
                progressBarLocation.visibility = if (it) View.VISIBLE else View.GONE
                if (it) {
                    findByLocationRV.visibility = View.VISIBLE
                    noDataFoundLocation.visibility = View.GONE
                }
            }
        })

        locationViewModel.locationList.observe(this, Observer { list ->
            list.let {
                findByLocationRV.visibility = View.VISIBLE
                findByLocationRV.adapter = context?.let { it1 -> LocationAdapter(it1, list) }
            }
        })
    }

}