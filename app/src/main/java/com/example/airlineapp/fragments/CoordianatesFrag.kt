package com.example.airlineapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.airlineapp.R
import com.example.airlineapp.adapter.CoordinateAdapter
import com.example.airlineapp.model.Coordinate
import com.example.airlineapp.util.LOG
import kotlinx.android.synthetic.main.fragment_coordianates.*
import org.json.JSONArray

class CoordianatesFrag : Fragment() {

    val coordinateArray = arrayListOf<Coordinate>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_coordianates, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val data = arguments?.get("data") as String
        LOG.e("data coordiantes.....$data")

        parseCoordinateList(data)
    }

    private fun parseCoordinateList(data: String?) {

        val level1Array = JSONArray(data)
        LOG.e("level 1 array.....$level1Array")
        val level2array = JSONArray(level1Array.getString(0))
        LOG.e("level 2 array.....$level2array")
        for (i in 0 until level2array.length()) {
            val level3array = JSONArray(level2array.getString(i))
            LOG.e("level 3 array.....$level3array")
            val coordinate = Coordinate()
            if (level3array.length() > 1) {
                coordinate.lat = level3array.getString(0)
                coordinate.long = level3array.getString(1)
            }
            coordinateArray.add(coordinate)

            setAdapter()

        }


        LOG.e("coordinate size...${coordinateArray.size}")

    }

    private fun setAdapter() {
        coordinatesRV.adapter = context?.let { it1 ->
            CoordinateAdapter(it1, coordinateArray)
        }

    }
}