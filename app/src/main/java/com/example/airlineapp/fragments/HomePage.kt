package com.example.airlineapp.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.NavDirections
import com.example.airlineapp.R
import com.example.airlineapp.adapter.EventAdapter
import com.example.airlineapp.model.HomePageData
import kotlinx.android.synthetic.main.fragment_home_page.*


class HomePage : Fragment() {

    var eventArray: ArrayList<HomePageData>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_page, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        createEvents()
    }

    private fun createEvents() {

        eventArray = ArrayList()
        addData(
            "Accident Statistics - Aggregated by the State of Occurrence",
            HomePageDirections.actionHomePage2ToAggregatedStateOfOccurenceFrag()
        )
        addData(
            "Accident Statistics - Aggregated by the State of Operator",
            HomePageDirections.actionHomePage2ToAggregatedStateOfOperator()
        )
        addData(
            "Accident Statistics - Flat by the State of Occurrence",
            HomePageDirections.actionHomePage2ToFlatStateOfOccurenceFrag()
        )
        addData(
            "Flight Information Regions - Geographical",
            HomePageDirections.actionHomePage2ToGeographicalFrag()
        )
        addData(
            "Flight Information Regions and Elevation - Find by Location",
            HomePageDirections.actionHomePage2ToFindByLocationFrag()
        )

        eventRV.adapter = context?.let { EventAdapter(it, eventArray!!) }
    }


    private fun addData(eventNAme: String, direction: NavDirections?) {
        val homePageData = HomePageData()
        homePageData.eventName = eventNAme
        homePageData.direction = direction
        eventArray?.add(homePageData)
    }
}