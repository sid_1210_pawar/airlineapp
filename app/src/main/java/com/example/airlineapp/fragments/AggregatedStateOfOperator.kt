package com.example.airlineapp.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.airlineapp.R
import com.example.airlineapp.adapter.AccidentStatAdapter
import com.example.airlineapp.model.AccidentalStatisticsData
import com.example.airlineapp.viewmodels.AccidentStatsViewModel
import kotlinx.android.synthetic.main.fragment_aggregated_state_of_occurence.*

class AggregatedStateOfOperator : Fragment() {

    lateinit var accidentStatsViewModel: AccidentStatsViewModel
    var stateOperaterList = arrayListOf<AccidentalStatisticsData>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_aggregated_state_of_operator, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        accidentStatsViewModel = ViewModelProviders.of(this).get(AccidentStatsViewModel::class.java)

        accidentStatsViewModel.fetchAccidentStatListForOperator(
            stateET.text.toString(),
            riskET.text.toString(),
            yearET.text.toString()
        )


        stateOperaterList.let {
            accidentStatsRV.adapter =
                context?.let { it1 ->
                    AccidentStatAdapter(it1, it)
                }
        }

        filterBtn.setOnClickListener {
            context?.let { it1 -> accidentStatsViewModel.hideKeyboardFrom(it1,it) }
            accidentStatsViewModel.fetchAccidentStatListForOperator(
                stateET.text.toString(),
                riskET.text.toString(),
                yearET.text.toString()
            )
        }

        resetBtn.setOnClickListener {
            context?.let { it1 -> accidentStatsViewModel.hideKeyboardFrom(it1,it) }
            stateET.setText("")
            riskET.setText("")
            yearET.setText("")
            accidentStatsViewModel.fetchAccidentStatListForOperator(
                stateET.text.toString(),
                riskET.text.toString(),
                yearET.text.toString()
            )
        }

        observeChanges()
    }

        private fun observeChanges() {

            accidentStatsViewModel.loadError.observe(this, Observer { error ->
                error.let {
                    noDataFound.visibility = if (it) View.VISIBLE else View.GONE
                }
            })

            accidentStatsViewModel.showProgress.observe(this, Observer { progress ->
                progress.let {
                    progressBar.visibility = if (it) View.VISIBLE else View.GONE
                    if (it) {
                        accidentStatsRV.visibility = View.VISIBLE
                        noDataFound.visibility = View.GONE
                    }
                }
            })

            accidentStatsViewModel.stateOperatorlist.observe(this, Observer { list ->
                list.let {
                    accidentStatsRV.visibility = View.VISIBLE
                    accidentStatsRV.adapter = context?.let { it1 -> AccidentStatAdapter(it1, list) }
                }
            })
        }


}