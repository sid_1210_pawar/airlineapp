package com.example.airlineapp.viewmodels

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.airlineapp.BaseClass
import com.example.airlineapp.model.AccidentalStatisticsData
import com.example.airlineapp.networking.ApiService
import com.example.airlineapp.util.LOG
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers


class AccidentStatsViewModel : ViewModel() {

    val loadError = MutableLiveData<Boolean>()
    val showProgress = MutableLiveData<Boolean>()

    var stateOccurencelist = MutableLiveData<ArrayList<AccidentalStatisticsData>>()
    var stateOperatorlist = MutableLiveData<ArrayList<AccidentalStatisticsData>>()
    var flatOccurenceList = MutableLiveData<ArrayList<AccidentalStatisticsData>>()

    private val apiService = ApiService(BaseClass.BASE_URL + "states/accidents/")
    private val disposable = CompositeDisposable()


    fun fetchAccidentStatList(states: String, risk: String, year: String) {
        showProgress.value = true
        disposable.add(
            apiService.getStateOfOccurenceList(states, risk, year)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ArrayList<AccidentalStatisticsData>>() {
                    override fun onSuccess(t: ArrayList<AccidentalStatisticsData>) {
                        showProgress.value = false
                        stateOccurencelist.value = t
                        loadError.value = false
                        LOG.e("Array Size occurence..." + t.size)
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        LOG.e("Failed occurence......." + e.printStackTrace())
                    }
                })
        )
    }


    fun fetchAccidentStatListForOperator(states: String, risk: String, year: String) {
        showProgress.value = true
        disposable.add(
            apiService.getStateOfOperatorist(states, risk, year)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ArrayList<AccidentalStatisticsData>>() {
                    override fun onSuccess(t: ArrayList<AccidentalStatisticsData>) {
                        showProgress.value = false
                        stateOperatorlist.value = t
                        loadError.value = false
                        LOG.e("Array Size operator..." + t.size)
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        LOG.e("Failed operator......." + e.printStackTrace())
                    }
                })
        )
    }


    fun fetchAccidentStatListForFlatOccurence(states: String, risk: String, year: String) {
        showProgress.value = true
        disposable.add(
            apiService.getFlatStateOccurenceList(states, risk, year)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ArrayList<AccidentalStatisticsData>>() {
                    override fun onSuccess(t: ArrayList<AccidentalStatisticsData>) {
                        showProgress.value = false
                        flatOccurenceList.value = t
                        loadError.value = false
                        LOG.e("Array Size flat occurence..." + t.size)
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        LOG.e("Failed flat occurence......." + e.printStackTrace())
                    }
                })
        )
    }


    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

}