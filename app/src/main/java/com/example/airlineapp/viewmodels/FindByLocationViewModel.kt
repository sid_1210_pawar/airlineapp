package com.example.airlineapp.viewmodels

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.airlineapp.BaseClass
import com.example.airlineapp.model.FinByLocationData
import com.example.airlineapp.model.GeographicalData
import com.example.airlineapp.networking.ApiService
import com.example.airlineapp.util.LOG
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

class FindByLocationViewModel : ViewModel() {
    val loadError = MutableLiveData<Boolean>()
    val showProgress = MutableLiveData<Boolean>()

    var locationList = MutableLiveData<ArrayList<FinByLocationData>>()
    var geographicalList = MutableLiveData<ArrayList<GeographicalData>>()

    private val apiService = ApiService(BaseClass.BASE_URL + "airspaces/zones/")
    private val disposable = CompositeDisposable()


    fun fetchByLocation(places: String) {
        showProgress.value = true
        disposable.add(
            apiService.getFindByLocation(places)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ArrayList<FinByLocationData>>() {
                    override fun onSuccess(t: ArrayList<FinByLocationData>) {
                        showProgress.value = false
                        locationList.value = t
                        loadError.value = false
                        LOG.e("Array Size location list..." + t.size)
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        LOG.e("Failed......." + e.printStackTrace())
                    }
                })
        )
    }


    fun fetchGeographicalData(firs: String) {
        showProgress.value = true
        disposable.add(
            apiService.getGeographicalData(firs)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object :
                    DisposableSingleObserver<ArrayList<GeographicalData>>() {
                    override fun onSuccess(t: ArrayList<GeographicalData>) {
                        showProgress.value = false
                        geographicalList.value = t

                        loadError.value = false
                        LOG.e("Array Size location list..." + t.size)
                    }

                    override fun onError(e: Throwable) {
                        loadError.value = true
                        showProgress.value = false
                        LOG.e("Failed......." + e.printStackTrace())
                    }
                })
        )
    }

    fun hideKeyboardFrom(context: Context, view: View) {
        val imm: InputMethodManager =
            context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
}