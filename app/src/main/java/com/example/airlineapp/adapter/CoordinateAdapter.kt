package com.example.airlineapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.airlineapp.R
import com.example.airlineapp.model.Coordinate
import com.example.airlineapp.util.LOG

class CoordinateAdapter(
    private var context: Context,
    private var list: ArrayList<Coordinate>
) : RecyclerView.Adapter<CoordinateAdapter.ViewHolderC>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderC {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.coordinate_rowitem, parent, false)
        return ViewHolderC(view)
    }

    fun updateList(newlist: List<Coordinate>) {
        newlist.let {
            list.clear()
            list.addAll(it)

            LOG.e("update list size..."+list.size)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: ViewHolderC, position: Int) {
        holder.latitude.setText(list[position].lat.toString())
        holder.longitude.setText(list[position].long.toString())


    }

    class ViewHolderC(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val latitude = itemView.findViewById<EditText>(R.id.latitude)
        val longitude = itemView.findViewById<EditText>(R.id.longitude)

    }
}