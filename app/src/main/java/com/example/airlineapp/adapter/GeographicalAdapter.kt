package com.example.airlineapp.adapter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.navigation.Navigation
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.airlineapp.R
import com.example.airlineapp.fragments.GeographicalFragArgs
import com.example.airlineapp.fragments.GeographicalFragDirections
import com.example.airlineapp.model.GeographicalData
import com.example.airlineapp.util.LOG

class GeographicalAdapter(
    private var context: Context,
    private var list: ArrayList<GeographicalData>
) : RecyclerView.Adapter<GeographicalAdapter.ViewHolderC>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderC {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.geogrpahical_rowitem, parent, false)
        return ViewHolderC(view)
    }

    fun updateList(newlist: List<GeographicalData>) {
        newlist.let {
            list.clear()
            list.addAll(it)

            LOG.e("update list size..." + list.size)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: ViewHolderC, position: Int) {
        holder.type.setText(list[position].type.toString())
        holder.centlat.setText(list[position].properties?.centlat.toString())
        holder.ICAOCODE.setText(list[position].properties?.iCAOCODE.toString())
        holder.REGION.setText(list[position].properties?.rEGION.toString())
        holder.FIRname.setText(list[position].properties?.fIRname.toString())
        holder.centlong.setText(list[position].properties?.centlong.toString())
        holder.StateCode.setText(list[position].properties?.stateCode.toString())
        holder.StateName.setText(list[position].properties?.stateName.toString())
        holder.typeGeometry.setText(list[position].geometry?.type.toString())
        holder.seeCoordiantesBtn.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("data",list[position].geometry?.coordinates.toString())
                val action =GeographicalFragArgs.fromBundle(bundle)
                it.findNavController().navigate(R.id.coordianatesFrag,bundle)
        }

    }

    class ViewHolderC(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val type = itemView.findViewById<EditText>(R.id.type)
        val centlat = itemView.findViewById<EditText>(R.id.centlat)
        val ICAOCODE = itemView.findViewById<EditText>(R.id.ICAOCODE)
        val REGION = itemView.findViewById<EditText>(R.id.REGION)
        val FIRname = itemView.findViewById<EditText>(R.id.FIRname)
        val centlong = itemView.findViewById<EditText>(R.id.centlong)
        val StateCode = itemView.findViewById<EditText>(R.id.StateCode)
        val StateName = itemView.findViewById<EditText>(R.id.StateName)
        val typeGeometry = itemView.findViewById<EditText>(R.id.typeGeometry)
        val seeCoordiantesBtn = itemView.findViewById<Button>(R.id.seeCoordiantesBtn)
    }
}