package com.example.airlineapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.airlineapp.R
import com.example.airlineapp.model.AccidentalStatisticsData
import com.example.airlineapp.util.LOG

class AccidentStatAdapter(
    private var context: Context,
    private var list: ArrayList<AccidentalStatisticsData>
) : RecyclerView.Adapter<AccidentStatAdapter.ViewHolderC>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderC {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.accidentstat_rowitem, parent, false)
        return ViewHolderC(view)
    }

    fun updateList(newlist: List<AccidentalStatisticsData>) {
        newlist.let {
            list.clear()
            list.addAll(it)

            LOG.e("update list size..."+list.size)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: ViewHolderC, position: Int) {
        holder.accidents.setText(list[position].accidents.toString())
        holder.fatalities.setText(list[position].fatalities.toString())
        holder.fatalAccidents.setText(list[position].fatalAccidents.toString())
        holder.year.setText(list[position].year.toString())
        holder.departures.setText(list[position].departures.toString())
        holder.accidentRate.setText(list[position].accidentRate.toString())

        if(list[position].states==null || list[position].states?.isEmpty()!!){
            holder.states.visibility = View.GONE
        }else{
            holder.states.visibility = View.VISIBLE
            holder.states.setText(list[position].states.toString())
        }
    }

    class ViewHolderC(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val accidents = itemView.findViewById<EditText>(R.id.accidents)
        val fatalities = itemView.findViewById<EditText>(R.id.fatalities)
        val fatalAccidents = itemView.findViewById<EditText>(R.id.fatalAccidents)
        val year = itemView.findViewById<EditText>(R.id.year)
        val departures = itemView.findViewById<EditText>(R.id.departures)
        val accidentRate = itemView.findViewById<EditText>(R.id.accidentRate)
        val states = itemView.findViewById<EditText>(R.id.states)
    }
}