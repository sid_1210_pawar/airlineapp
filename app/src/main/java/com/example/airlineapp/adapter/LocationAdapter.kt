package com.example.airlineapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.recyclerview.widget.RecyclerView
import com.example.airlineapp.R
import com.example.airlineapp.model.FinByLocationData
import com.example.airlineapp.util.LOG

class LocationAdapter(
    private var context: Context,
    private var list: ArrayList<FinByLocationData>
) : RecyclerView.Adapter<LocationAdapter.ViewHolderC>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderC {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.finbylocation_rowitem, parent, false)
        return ViewHolderC(view)
    }

    fun updateList(newlist: List<FinByLocationData>) {
        newlist.let {
            list.clear()
            list.addAll(it)

            LOG.e("update list size..."+list.size)
            notifyDataSetChanged()
        }

    }

    override fun getItemCount() = list.size


    override fun onBindViewHolder(holder: ViewHolderC, position: Int) {
        holder.latitude.setText(list[position].latitude.toString())
        holder.longitude.setText(list[position].longitude.toString())
        holder.elevation.setText(list[position].elevation.toString())
        holder.firname.setText(list[position].FIRname.toString())
        holder.fircode.setText(list[position].FIRcode.toString())
        holder.region.setText(list[position].region.toString())

    }

    class ViewHolderC(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val latitude = itemView.findViewById<EditText>(R.id.latitude)
        val longitude = itemView.findViewById<EditText>(R.id.longitude)
        val elevation = itemView.findViewById<EditText>(R.id.elevation)
        val firname = itemView.findViewById<EditText>(R.id.firname)
        val fircode = itemView.findViewById<EditText>(R.id.fircode)
        val region = itemView.findViewById<EditText>(R.id.region)
    }
}