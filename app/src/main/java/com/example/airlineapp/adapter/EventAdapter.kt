package com.example.airlineapp.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.airlineapp.R
import com.example.airlineapp.model.HomePageData

class EventAdapter(
    private var context: Context,
    private var list: ArrayList<HomePageData>
) : RecyclerView.Adapter<ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.homepage_items, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.eventName.text = list[position].eventName
        holder.itemView.setOnClickListener {
            list[position].direction?.let { it1 -> Navigation.findNavController(it).navigate(it1) }
        }
    }


}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val eventName = itemView.findViewById<TextView>(R.id.eventName)
}