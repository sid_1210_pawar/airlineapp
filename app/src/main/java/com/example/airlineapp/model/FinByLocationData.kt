package com.example.airlineapp.model

data class FinByLocationData(
	val elevation: String? = null,
	val FIRcode: String? = null,
	val FIRname: String? = null,
	val latitude: Int? = null,
	val region: String? = null,
	val longitude: Int? = null
)

