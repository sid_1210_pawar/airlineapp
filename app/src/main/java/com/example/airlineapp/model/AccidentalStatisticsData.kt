package com.example.airlineapp.model

import com.google.gson.annotations.SerializedName

data class AccidentalStatisticsData(

	@field:SerializedName("AccidentRate")
	val accidentRate: Double? = null,

	@field:SerializedName("Year")
	val year: Int? = null,

	@field:SerializedName("FatalAccidents")
	val fatalAccidents: Int? = null,

	@field:SerializedName("Fatalities")
	val fatalities: Int? = null,

	@field:SerializedName("Departures")
	val departures: Int? = null,

	@field:SerializedName("State")
	val states: String? = null,

	@field:SerializedName("Accidents")
	val accidents: Int? = null
)
