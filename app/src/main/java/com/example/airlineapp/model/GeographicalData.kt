package com.example.airlineapp.model

import com.google.gson.JsonArray
import com.google.gson.annotations.SerializedName
import org.json.JSONArray

data class GeographicalData(

    @field:SerializedName("geometry")
    val geometry: Geometry? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("properties")
    val properties: Properties? = null
)

data class Geometry(

    @field:SerializedName("coordinates")
    val coordinates: JsonArray? = null,

    @field:SerializedName("type")
    val type: String? = null
)

data class Properties(

    @field:SerializedName("centlong")
    val centlong: Double? = null,

    @field:SerializedName("centlat")
    val centlat: Double? = null,

    @field:SerializedName("FIRname")
    val fIRname: String? = null,

    @field:SerializedName("StateName")
    val stateName: String? = null,

    @field:SerializedName("StateCode")
    val stateCode: String? = null,

    @field:SerializedName("ICAOCODE")
    val iCAOCODE: String? = null,

    @field:SerializedName("REGION")
    val rEGION: String? = null
)
