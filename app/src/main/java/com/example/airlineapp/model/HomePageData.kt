package com.example.airlineapp.model

import androidx.navigation.NavDirections

class HomePageData {

    var eventName: String? = ""
    var direction: NavDirections? = null
}